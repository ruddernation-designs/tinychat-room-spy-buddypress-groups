=== TinyChat Room Spy - Buddypress Groups - On GitHub Only! ===

Contributors: Ruddernation Designs
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FFAC7FBEBH6JE
Tags: buddypress tinychat room spy, buddypress room spy, tinychat room spy, ruddernation, room spy, roomspy, tinychat
Requires at least: 3.6.0
Tested up to: 4.4
Stable tag: 1.0.5
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Description ==

This plugin is used for BuddyPress to add a TinyChat room spy to their groups, Group creators can enable this in the admin section,
This allows you to view who is in a room before you enter, It will also tell you if there are any moderators in the room, How many is on video/audio and number of chatters, This now shows the room with the same name as the group!
so if the group is called 'Monachechat4' It will show that room, There is no search for this now.

== Installation ==

* Upload the plugin to your plugins folder then activate,
* Create a new group and click enable TinyChat Room Spy or go to an already created group and enable TinyChat Room Spy in the admin area.

== Frequently Asked Questions ==

* Q. Can I use this if I'm not logged in?
* A. Yes.

== Changelog ==

= 1.0.0 =
* first version.

= 1.0.1 =
* Removal of "Site Wide Only" to Network: True.

= 1.0.3 = 
* Removal of form and set to group name only! so for this to view a room the the group should have the same name as the room name, This does not have to be case sensitive.

= 1.0.5 =
* Minor update to let room spy refresh automatically every 20 seconds so no need to do that manually.

== Social Sites ==

* Website - https://www.ruddernation.com

* Room Spy - https://www.tinychat-spy.com/

* Facebook - https://www.facebook.com/rndtc

* Github - https://github.com/ruddernation

* GitHub Repositories - https://ruddernation-designs.github.io

* Google Plus - https://plus.google.com/+Ruddernation/posts

* Google Community - https://plus.google.com/communities/100073731487570686181

* Twitter - https://twitter.com/_ruddernation_
