<?php

/*
* Plugin Name: TinyChat Room Spy - Buddypress Groups - Only available on GitHub!
* Plugin URI: https://github.com/Ruddernation-Designs/tinychat-room-spy-buddypress-groups
* Author: Ruddernation Designs
* Author URI: https://github.com/Ruddernation-Designs
* Description: This plugin is used for BuddyPress to add a TinyChat room spy to their groups, Group creators can enable this in the admin section,
This allows you to view who is in a room before you enter, It will also tell you if there are any moderators in the room, How many is on video/audio and number of chatters, This now shows the room with the same name as the group!
so if the group is called 'Monachechat4' It will show that room, There is no search for this now.
* Version: 1.0.5
* Requires at least: WordPress 4.0, BuddyPress 2.0
* Tested up to: WordPress 4.4, BuddyPress 2.5
* Network: true
* Date: 26th March 2016
* License: GPLv3
* License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/
function bp_buddypress_tinychat_roomspy_init() {
	    global $wpdb;
    if ( is_multisite() && BP_ROOT_BLOG != $wpdb->blogid ) {
	return;
    }
    if ( ! bp_is_active( 'groups' ) ) {
	return;
    }
	require( dirname( __FILE__ ) . '/core.php' );}
add_action( 'bp_include', 'bp_buddypress_tinychat_roomspy_init' , 85);
function bp_buddypress_tinychat_roomspy_activate() {
	global $wpdb;
	if ( !empty($wpdb->charset) )
		$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
$sql[] = "CREATE TABLE {$wpdb->base_prefix}bp_buddypress_tinychat_roomspy (
id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
group_id bigint(20) NOT NULL,
user_id bigint(20) NOT NULL,
message_content text) {$charset_collate};";
	$sql[] = "CREATE TABLE {$wpdb->base_prefix}bp_buddypress_tinychat_roomspy_online (
id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
group_id bigint(20) NOT NULL,
user_id bigint(20) NOT NULL,
timestamp int(11) NOT NULL) {$charset_collate};";
	require_once( ABSPATH . 'wp-admin/upgrade-functions.php' );
	dbDelta($sql);
	update_site_option( 'bp-buddypress_tinychat_roomspy-db-version', BP_TINYCHAT_GROUP_CHAT_DB_VERSION );
}
register_activation_hook( __FILE__, 'bp_buddypress_tinychat_roomspy_activate' ); ?>
